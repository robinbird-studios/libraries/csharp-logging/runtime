using System.Collections.Generic;

namespace RobinBird.Logging.Unity
{
	/// <summary>
	/// Provider which specifies what parts of the application are allowed to show verbose logs
	/// </summary>
	public interface IVerboseLoggingSettings
	{
		bool AllowAll { get; }
		ICollection<string> VerboseFilePaths { get; }
		ICollection<string> VerboseCategories { get; }
		ICollection<string> VerboseMembers { get; }
	}
}